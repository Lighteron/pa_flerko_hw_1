# Дополнить декоратор сache поддержкой max_limit.
# Алгоритм кеширования LFU: https://en.wikipedia.org/wiki/Least_frequently_used

import functools


def cache(limit=10):
    def profile(f):
        @functools.wraps(f)
        def deco(*args, **kwargs):
            keys = (args, tuple(kwargs.items()))
            for elem in bd:
                if elem["key"] == keys:
                    elem["counter"] += 1
                    return elem["data"]
            result = f(keys)
            if len(bd) >= limit:
                bd.sort(key=lambda i: i["counter"], reverse=True)
                bd.pop()
            bd.append({"key": keys, "data": result, "counter": 1})
            return result
        bd = []                 # [{key: keys, data: result, counter: 0}]
        return deco
    return profile

